TBoT for RSC#199
================

A simple RuneScape Classic bot, originally written by RichyT based on
a deobfuscated client released by saevion.

Basic usage
-----------

Run with `ant run`, compile scripts with `ant compileScripts`.

Requires an external fatigue OCR (i.e. LeoSleep/WUSS) that supports the
`HC.BMP`/`slworld.txt` standard.

Run scripts by specifying their command name (see each script file)
in the in-game chat prefixed with ##. Paramters are separated with #.

e.g. to thieve men:

	##thief 11

Type `##stop` to stop the script.

Keyboard bindings
-----------------

* Up/Down - adjust camera pitch
* PgUp/PgDown - adjust camera zoom
* F1 - toggle interlacing, as with vanilla clients
* F2 - toggle fog of war
* F3 - toggle graphical rendering
* F4 - records player steps to console, press F4 again to stop
* F12 - enable/disable autologin
